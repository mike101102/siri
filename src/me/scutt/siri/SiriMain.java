package me.scutt.siri;

import java.util.ArrayList;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class SiriMain extends JavaPlugin {

    public void onEnable() {
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(new MyPluginListener(this), this);
    }

    public void onDisable() {

    }

    public void send(String message) {
        System.out.println("[Siri] " + message);
    }

    public ArrayList<ArrayList<String>> getKeywords() {
        ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
        Set<String> configlist = getConfig().getConfigurationSection("keywords").getKeys(false);
        if ((configlist == null) || (configlist.size() < 1))
            return list;
        for (String i : configlist) {
            ArrayList<String> l = new ArrayList<String>();
            String[] a = i.split(",");
            for (int b = 0; b < a.length; b++) {
                l.add(a[b]);
            }
            list.add(l);
        }
        return list;
    }

    public String getPrefix() {
        return convert(getConfig().getString("prefix") + " ");
    }

    public String getResponse(String keywords) {
        return getConfig().getString("keywords." + keywords + ".response");
    }

    public Boolean shouldCancel(String keywords) {
        return Boolean.valueOf(getConfig().getBoolean("keywords." + keywords + ".cancel-message", true));
    }

    public boolean shouldBroadcast(Player player, String keywords) {
        return (player.hasPermission("Siri.broadcast")) && (getConfig().getBoolean("broadcast-admin-replies", true)) && (!getConfig().getBoolean("keywords." + keywords + ".cancel-message"));
    }

    public String convert(String string) {
        return ChatColor.translateAlternateColorCodes("^".charAt(0), string);
    }

    private void help(CommandSender s) {
        s.sendMessage(getPrefix() + "Commands You Can Use");
        if (s.hasPermission("Siri.reload"))
            s.sendMessage(getPrefix() + "/siri reload");
    }

    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
        if (args.length == 0) {
            help(sender);
            return true;
        }

        if (args[0].equalsIgnoreCase("reload")) {
            if (sender.hasPermission("Siri.reload")) {
                this.reloadConfig();
                sender.sendMessage(getPrefix() + "Reload complete");
                return true;
            } else {
                sender.sendMessage(ChatColor.RED + "You do not have permission (Siri.reload)");
                return true;
            }
        }
        return false;
    }
}