package me.scutt.siri;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

public class MyPluginListener implements Listener {

    private SiriMain plugin;

    public MyPluginListener(SiriMain plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerEnterVoid(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (player.getLocation().getY() < 0.0D) {
            player.teleport(player.getWorld().getSpawnLocation());
            player.sendMessage(plugin.getPrefix() + "Try not to jump down there, I hate having to catch you delinquants all the time.");
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerJoin(final PlayerJoinEvent event) {
        this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable() {
            public void run() {
                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.AMBIENCE_CAVE, 1.0F, 1.0F);
                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.NOTE_BASS_DRUM, 1.0F, 10.0F);
            }
        }, 14L);
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onBucketUse(PlayerBucketEmptyEvent event) {
        if (event.getBucket() == Material.LAVA_BUCKET) {
            Player player = event.getPlayer();
            if (!player.hasPermission("Siri.placelava")) {
                event.setCancelled(true);
                player.setItemInHand(new ItemStack(Material.DIRT, 1));
                this.plugin.getServer().broadcastMessage(plugin.getPrefix() + event.getPlayer().getDisplayName() + "', Tried to place Lava but I was able to drain the liquid before serious damage was caused.");
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        String message = event.getMessage().toLowerCase();
        if (!event.getPlayer().hasPermission("Siri.answer"))
            return;
        ArrayList<String> acceptedKeys = new ArrayList<String>();
        boolean answer;
        for (ArrayList<String> a : plugin.getKeywords()) {
            answer = true;
            for (String b : a) {
                if (!message.contains(b.toLowerCase())) {
                    answer = false;
                    break;
                }
            }
            if (answer) {
                StringBuilder sb = new StringBuilder();
                for (String b : a) {
                    sb.append(b + ",");
                }
                sb.deleteCharAt(sb.length() - 1);
                acceptedKeys.add(sb.toString());
            }
        }
        if (acceptedKeys.size() > 0) {
            String largestSb = "";
            for (String i : acceptedKeys) {
                if (i.length() > largestSb.length()) {
                    largestSb = i;
                }
            }
            if (this.plugin.shouldCancel(largestSb).booleanValue()) {
                event.setCancelled(true);
            }
            String response = this.plugin.getResponse(largestSb);
            if (response == null)
                return;
            response = response.replaceAll("%name%", event.getPlayer().getName());
            if (response.startsWith("/")) {
                if (response.toLowerCase().contains("%player%")) {
                    String[] cutup = message.split(" ");
                    for (int i = cutup.length; i > 0; i--) {
                        Player player = this.plugin.getServer().getPlayer(cutup[(i - 1)]);
                        if (player != null) {
                            this.plugin.send("Found: " + player.getName());
                            response = response.toLowerCase().replaceAll("%player%", player.getName());
                            break;
                        }
                    }
                }
                this.plugin.send("Executing: " + response.replaceAll("/", ""));
                if (!response.toLowerCase().contains("%console%")) {
                    if (this.plugin.shouldBroadcast(event.getPlayer(), largestSb))
                        delayBroadcast(plugin.getPrefix() + ChatColor.GOLD + "Executing " + ChatColor.GREEN + "/" + response.replaceAll("/", "").replaceAll("%console%", "") + ChatColor.GOLD + " for player " + event.getPlayer().getDisplayName(), 14L);
                    else {
                        delayMessage(plugin.getPrefix() + ChatColor.GOLD + "Executing the command for you", event.getPlayer(), 14L);
                    }
                    this.plugin.getServer().dispatchCommand(event.getPlayer(), response.replaceAll("/", ""));
                    return;
                }
                if (event.getPlayer().hasPermission("Siri.consolecommand")) {
                    if (this.plugin.shouldBroadcast(event.getPlayer(), largestSb))
                        delayBroadcast(plugin.getPrefix() + ChatColor.GOLD + "Executing " + ChatColor.GREEN + "/" + response.replaceAll("/", "").replaceAll("%console%", "") + ChatColor.GOLD + " as the console for player " + event.getPlayer().getDisplayName(), 14L);
                    else {
                        delayMessage(plugin.getPrefix() + ChatColor.GOLD + "Executing the command as the console", event.getPlayer(), 14L);
                    }
                    this.plugin.getServer().dispatchCommand(this.plugin.getServer().getConsoleSender(), response.replaceAll("/", "").replaceAll("%console%", ""));
                    return;
                }
                event.getPlayer().sendMessage(ChatColor.RED + "You do not have permission (Siri.consolecommand)");
                event.setCancelled(true);
                return;
            }

            if (this.plugin.shouldBroadcast(event.getPlayer(), largestSb))
                delayBroadcast(plugin.getPrefix() + this.plugin.convert(response), 14L);
            else {
                delayMessage(plugin.getPrefix() + this.plugin.convert(response), event.getPlayer(), 14L);
            }
            return;
        }
    }

    public void delayMessage(final String message, final Player player, long delay) {
        this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable() {
            public void run() {
                player.sendMessage(message);
            }
        }, delay);
    }

    public void delayBroadcast(final String broadcast, long delay) {
        this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable() {
            public void run() {
                Bukkit.broadcastMessage(broadcast);
            }
        }, delay);
    }
}